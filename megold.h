#ifndef MEGOLD_H_INCLUDED
#define MEGOLD_H_INCLUDED

typedef struct DinTomb {
    int *adat;
    int meret;
} DinTomb;

void dintomb_foglal(DinTomb *dt, int meret);
void dintomb_felszabadit(DinTomb *dt);
int max_elem(int **matrix,int i, int j, int x);
int nulla_kereses (int **matrix,int i, int j, int x, DinTomb *dt);
void beir (int **matrix,int i, int j, int x, DinTomb *dt, int szamlalo);
void beirkeres (int **matrix,int i, int j, int x, DinTomb *dt, DinTomb *dt2, int szamlalo);




#endif // MEGOLD_H_INCLUDED
