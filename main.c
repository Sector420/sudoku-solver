#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "beolvas.h"
#include "megold.h"
#include "debugmalloc.h"

int main(void) {
    int **matrix;
    int x, y;
    matrix_beolvas(&matrix, "25x25elso.txt", &x, &y);

    for(int i=0;i<y;i++)
    {
        for(int j=0;j<x;j++)
        {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    //printf("ez az x %d \n", x);
    int szamlalo=0;
    int max=0;
    bool nincs_nulla=false;
    while(!(nincs_nulla))
    {
    for (int i=0;i<x;i++)
    {
        for(int j=0;j<y;j++)
        {
            if(matrix[i][j]==0)
            {
                max=max_elem(matrix,i,j,x);
                //printf("ez a maxelem %d \n", max);
                DinTomb dt;
                //printf("ennyit kell foglalni %d \n",x-1);
                dintomb_foglal(&dt, max);
                szamlalo=nulla_kereses(matrix,i,j,x,&dt);
                if(szamlalo==x-1)
                {
                    beir(matrix,i,j,x,&dt,szamlalo);
                }
                else if(szamlalo<x-1)
                {
                    DinTomb dt2;
                    //printf("ennyi a max %d \n",max);
                    //printf("ennyi a szamlalo %d \n",szamlalo);
                    //printf("ennyit kell foglalni a masik tombnek %d \n", (x-1)-szamlalo+1);
                    dintomb_foglal(&dt2, (x-1)-szamlalo+1);
                    beirkeres(matrix,i,j,x,&dt,&dt2,szamlalo);
                    dintomb_felszabadit(&dt2);
                }
                dintomb_felszabadit(&dt);
            }
        }
    }
    /*for(int i=0;i<x;i++)
    {
        for(int j=0;j<x;j++)
        {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");*/
    szamlalo=0;
    bool talalt_nullat=false;
    for(int i=0;i<x;i++)
    {
        for(int j=0;j<y;j++)
        {
            if(matrix[i][j]==0)
            {
                talalt_nullat=true;
            }
        }
    }
    if (talalt_nullat==false)
    {
        nincs_nulla=true;
    }
    }
    for(int i=0;i<y;i++)
    {
        for(int j=0;j<x;j++)
        {
            printf("%2d ", matrix[i][j]);
        }
        printf("\n");
    }

    for(int i=0;i<y;i++)
    {
        free(matrix[i]);
    }
    free(matrix);

    return 0;
}
