#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "beolvas.h"
#include "debugmalloc.h"

bool sor_beolvas(FILE *f, int **sor, int *x)
{
    *sor=NULL;
    int x2=0;
    int t;
    char c;
    while(fscanf(f, "%d%c", &t, &c) != EOF)
    {
        x2+=1;
        *sor = (int*) realloc(*sor, x2*(sizeof(int)));
        (*sor)[x2-1]=t;
        if(c == '\n')
        {
            *x=x2;
            return true;
        }
    }
    return false;
}

void matrix_beolvas(int ***matrix, char *filename, int *x, int *y)
{
    *matrix = NULL;
    *y = 0;
    FILE *f=fopen(filename, "r");
    if (f != NULL)
    {
        int* sor;
        while(sor_beolvas(f, &sor, x))
        {
            *y+=1;
            *matrix=(int**) realloc(*matrix, *y*sizeof(int*));
            (*matrix)[(*y)-1]=sor;
        }
    }
    else
    {
        perror("Nem sikerült megnyitni a fájlt");
    }
    fclose(f);
}
