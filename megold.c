#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "megold.h"
#include "debugmalloc.h"

void dintomb_foglal(DinTomb *dt, int meret)
{
    dt->meret = meret;
    dt->adat = (int*) malloc(meret * sizeof(int));
    return dt->adat != NULL;
}

void dintomb_felszabadit(DinTomb *dt)
{
    free(dt->adat);
}

int max_elem(int **matrix,int i, int j, int x)
{
    int maxelem=0;
    int atmenet=0;
    for(int z=0;z<x;z++)
    {
        if(matrix[i][z]!=0)
        {
            atmenet=matrix[i][z];
            bool talalt=false;
            for(int l=0;l<z;l++)
            {
                if(matrix[i][l]==atmenet)
                {
                    talalt=true;
                }
            }
            if(talalt==false)
            {
                maxelem++;
            }
        }
    }
    for(int z=0;z<x;z++)
    {
        if(matrix[z][j]!=0)
        {
            atmenet=matrix[z][j];
            bool talalt=false;
            for(int l=0;l<x;l++)
            {
                if(matrix[i][l]==atmenet)
                {
                    talalt=true;
                }
            }
            for(int l=0;l<z;l++)
            {
                if(matrix[l][j]==atmenet)
                {
                    talalt=true;
                }
            }
            if(talalt==false)
            {
                maxelem++;
            }
        }
    }
    int hanyadik_reszt_sor=0;
    int hanyadik_reszt_osz=0;
    int ossz_reszt=sqrt(x);
    hanyadik_reszt_sor=i/ossz_reszt;
    hanyadik_reszt_osz=j/ossz_reszt;
    for (int z1=0+ossz_reszt*hanyadik_reszt_sor;z1<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;z1++)
    {
        for (int z2=0+ossz_reszt*hanyadik_reszt_osz;z2<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;z2++)
        {
            if(matrix[z1][z2]!=0)
            {
                atmenet=matrix[z1][z2];
                bool talalt=false;
                for(int l=0;l<x;l++)
                {
                    if(matrix[i][l]==atmenet)
                    {
                        talalt=true;
                    }
                }
                for(int l=0;l<x;l++)
                {
                    if(matrix[l][j]==atmenet)
                    {
                        talalt=true;
                    }
                }
                for (int l1=0+ossz_reszt*hanyadik_reszt_sor;l1<z1;l1++)
                {
                    for (int l2=0+ossz_reszt*hanyadik_reszt_osz;l2<z2;l2++)
                    {
                        if(matrix[l1][l2]==atmenet)
                        {
                            talalt=true;
                        }
                    }
                }
                if(talalt==false)
                {
                    maxelem++;
                }
            }
        }
    }
    return maxelem;
}

int *kereses_sor(int **matrix,int i, int j, int x, DinTomb *dt, int *szamlalo, int mitkeres)
{
    int t=0;
    for(int z=0;z<x;z++)
    {
        if(matrix[i][z]!=mitkeres)
        {
            while (dt->adat[t]!=matrix[i][z] && t<=*szamlalo)
            {
                t++;
            }
            if (t>*szamlalo)
            {
                *szamlalo+=1;;
                dt->adat[*szamlalo]=matrix[i][z];
            }
            t=0;
        }
    }
    return *szamlalo;
}

int *kereses_oszlop(int **matrix,int i, int j, int x, DinTomb *dt, int *szamlalo, int mitkeres)
{
    int t=0;
    for(int z=0;z<x;z++)
    {
        if(matrix[z][j]!=mitkeres)
        {
            while (dt->adat[t]!=matrix[z][j] && t<=*szamlalo)
            {
                t++;
            }
            if (t>*szamlalo)
            {
                *szamlalo+=1;
                dt->adat[*szamlalo]=matrix[z][j];
            }
            t=0;
        }
    }
    return *szamlalo;
}

int *kereses_resztablazat(int **matrix,int i, int j, int x, DinTomb *dt, int *szamlalo, int mitkeres)
{
    int t=0;
    int hanyadik_reszt_sor=0;
    int hanyadik_reszt_osz=0;
    int ossz_reszt=sqrt(x);
    hanyadik_reszt_sor=i/ossz_reszt;
    hanyadik_reszt_osz=j/ossz_reszt;
    for (int z=0+ossz_reszt*hanyadik_reszt_sor;z<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;z++)
    {
        for (int l=0+ossz_reszt*hanyadik_reszt_osz;l<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;l++)
        {
            if(matrix[z][l]!=mitkeres)
            {
                while (dt->adat[t]!=matrix[z][l] && t<=*szamlalo)
                {
                    t++;
                }
                if (t>*szamlalo)
                {
                    *szamlalo+=1;
                    dt->adat[*szamlalo]=matrix[z][l];
                }
                t=0;
            }
        }
    }
    return *szamlalo;
}
int nulla_kereses (int **matrix,int i, int j, int x, DinTomb *dt)
{
    int t=0;
    int szamlalo;
    szamlalo=-1;
    kereses_sor(matrix,i,j,x,dt,&szamlalo,0);
    kereses_oszlop(matrix,i,j,x,dt,&szamlalo,0);
    kereses_resztablazat(matrix,i,j,x,dt,&szamlalo,0);
    /*for(int i=0;i<=szamlalo;i++)
    {
        printf("%d ",dt->adat[i]);
    }
    printf("\n");
    printf("ez a szamlalo %d \n", szamlalo);*/
    szamlalo+=1;
    return szamlalo;
}

void beir (int **matrix,int i, int j, int x, DinTomb *dt, int szamlalo)
{
    int t=0;
    int r=0;
    bool van=false;
    while(!van)
    {
        r++;
        while (dt->adat[t]!=r && t<szamlalo)
        {
            t++;
        }
        if(t==szamlalo)
        {
            matrix[i][j]=r;
            van=true;
        }
        t=0;
    }
}

void beirkeres (int **matrix,int i, int j, int x, DinTomb *dt, DinTomb *dt2, int szamlalo)
{
    //printf("ez a szamlalo %d \n", szamlalo);
    int t=0;
    for(int z=1;z<=x;z++)
    {
        bool vanbenne=false;
        for(int l=0;l<szamlalo;l++)
        {
            //printf("ez az l %d \n",l);
            if(dt->adat[l]==z)
            {
                vanbenne=true;
            }
        }
        if(vanbenne==false)
        {
            //printf("ez z %d \n",z);
            dt2->adat[t]=z;
            t++;
            //printf("ez a szam %d \n",dt2->adat[t]);
            //printf("ez a t %d \n",t);
        }
    }
    t=t-1;
    //printf("vegso t %d \n",t);
    /*for(int i=0;i<szamlalo;i++)
    {
        printf("%d ", dt->adat[i]);
    }
    printf("\n");*/
    /*for(int i=0;i<=t;i++)
    {
        printf("%d ", dt2->adat[i]);
    }
    printf("\n");*/
    int hanynulla=0;
    int hanykeresett=0;
    for(int l=0;l<=t;l++)
    {
        for(int z=0;z<x;z++)
        {
            if(matrix[i][z]==0 && j!=z)
            {
                hanynulla+=1;
            }
        }
        bool talalt=false;
        int hanyadik_reszt_sor=0;
        int hanyadik_reszt_osz=0;
        int ossz_reszt=sqrt(x);
        int p=0;
        while (p<x)
        {
            hanyadik_reszt_sor=i/ossz_reszt;
            hanyadik_reszt_osz=p/ossz_reszt;
            for (int u1=0+ossz_reszt*hanyadik_reszt_sor;u1<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;u1++)
            {
                for (int u2=0+ossz_reszt*hanyadik_reszt_osz;u2<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;u2++)
                {
                    if(matrix[u1][u2]==dt2->adat[l])
                    {
                        talalt=true;
                        for (int r=0+ossz_reszt*hanyadik_reszt_osz;r<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;r++)
                        {
                            if(matrix[i][r]==0)
                            {
                                hanykeresett++;
                            }
                        }
                    }
                }
            }
            if(talalt==false)
            {
                for (int r1=0+ossz_reszt*hanyadik_reszt_osz;r1<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;r1++)
                {
                    if (matrix[i][r1]==0 && j!=r1)
                    {
                        for (int r2=0;r2<x;r2++)
                        {
                            if(matrix[r2][r1]==dt2->adat[l])
                            {
                                hanykeresett++;
                            }
                        }
                    }
                }
            }
            talalt=false;
            p+=ossz_reszt;
        }
        if(hanykeresett==hanynulla)
        {
            matrix[i][j]=dt2->adat[l];
        }
        hanynulla=0;
        hanykeresett=0;
        for(int z=0;z<x;z++)
        {
            if(matrix[z][j]==0 && i!=z)
            {
                hanynulla+=1;
            }
        }
        p=0;
        while (p<x)
        {
            hanyadik_reszt_sor=p/ossz_reszt;
            hanyadik_reszt_osz=j/ossz_reszt;
            for (int u1=0+ossz_reszt*hanyadik_reszt_sor;u1<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;u1++)
            {
                for (int u2=0+ossz_reszt*hanyadik_reszt_osz;u2<ossz_reszt+ossz_reszt*hanyadik_reszt_osz;u2++)
                {
                    if(matrix[u1][u2]==dt2->adat[l])
                    {
                        talalt=true;
                        for (int r=0+ossz_reszt*hanyadik_reszt_sor;r<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;r++)
                        {
                            if(matrix[r][j]==0)
                            {
                                hanykeresett++;
                            }
                        }
                    }
                }
            }
            if(talalt==false)
            {
                for (int r1=0+ossz_reszt*hanyadik_reszt_sor;r1<ossz_reszt+ossz_reszt*hanyadik_reszt_sor;r1++)
                {
                    if (matrix[r1][j]==0 && i!=r1)
                    {
                        for (int r2=0;r2<x;r2++)
                        {
                            if(matrix[r1][r2]==dt2->adat[l])
                            {
                                hanykeresett++;
                            }
                        }
                    }
                }
            }
            talalt=false;
            p+=ossz_reszt;
        }
        if(hanykeresett==hanynulla)
        {
            matrix[i][j]=dt2->adat[l];
        }
        hanynulla=0;
        hanykeresett=0;
    }
}
