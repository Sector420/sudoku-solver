#ifndef BEOLVAS_H_INCLUDED
#define BEOLVAS_H_INCLUDED

bool sor_beolvas(FILE *f, int **sor, int *x);
void matrix_beolvas(int ***matrix, char *filename, int *x, int *y);


#endif // BEOLVAS_H_INCLUDED
